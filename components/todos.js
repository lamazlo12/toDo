import React from 'react';
import { StyleSheet, Text, View, ListView, TextInput, TouchableOpacity } from 'react-native';
const Dimensions = require('Dimensions');
const window = Dimensions.get('window');
import firebase from 'firebase';
import Swipeout from 'react-native-swipeout';

const firebaseConfig = {
  apiKey: "AIzaSyAinjGkh4pdbQDObK9RLgqQuA0r5hhSfJ0",
    authDomain: "projektpo-71943.firebaseapp.com",
    databaseURL: "https://projektpo-71943.firebaseio.com",
    projectId: "projektpo-71943",
    storageBucket: "projektpo-71943.appspot.com"
}

const firebaseApp = firebase.initializeApp(firebaseConfig);

export default class Todos extends React.Component {
  constructor(){
    super();
    let ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!==r2});
    this.state = {
      itemDataSource: ds,
      text:'',
    }
    this.itemsRef = this.getRef().child('items');
    this.renderRow = this.renderRow.bind(this);
  }

 getRef(){
    return firebaseApp.database().ref();
  }

  addItem(){
  if(this.state.text){
    this.itemsRef.push({title: this.state.text});
    this.setState({text:''});

    console.log(this.state.text);
  }
  }

  componentWillMount(){
    this.getItems(this.itemsRef);
  }

  /*componentDidMount(){
    this.getItems(this.itemsRef);
  }*/


  renderRow(item){
    const swipeSettings = {
      backgroundColor:'#fff',
      autoClose: true,
      marginTop:10,
      onClose: (secId,rowId,direction)=>{

      },
      onOpen: (secId,rowId,direction)=>{

      },
      right: [
        {
          onPress: ()=>{
            return firebase.database().ref('items/'+item._key).remove();
          },
          text:'Delete',type:'delete'
        }
      ],
    };
    
    return(
      <Swipeout {...swipeSettings}>
      <View style={styles.todo}>
        <Text style={styles.todoTitle}>{item.title}</Text>
      </View>
      </Swipeout>
    );
  }

  getItems(itemsRef){
    itemsRef.on('value',(snap)=>{
      let items = [];
      snap.forEach((child)=>{
        items.push({
          title: child.val().title,
          _key: child.key
        });
      });
      this.setState({
      itemDataSource: this.state.itemDataSource.cloneWithRows(items)
    });
    });

  }
  render() {
    return (
      <View style={styles.container}>
      <ListView
        dataSource={this.state.itemDataSource}
        renderRow={this.renderRow}
        />
        <View  style={styles.containerr}>
          <View style={styles.inputContainer}>
            <TextInput style={styles.input}

              placeholder="Add To Do"

              maxLength = {30}
              underlineColorAndroid="#3ABCA7"
              onChangeText = {(value)=>this.setState({text: value})}
              value={this.state.text}
              />
          </View>
            <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.button}
              onPress={()=>this.addItem()}
              >
              <Text style={styles.plus}>+</Text>
            </TouchableOpacity>
            </View>
        </View>
    </View>
    );
  }
}

/*class AddButton extends React.Component {
  render() {
    return (
      <View  style={styles.containerr}>
        <View style={styles.inputContainer}>
          <TextInput style={styles.input}

            placeholder="Add To Do"
            maxLength = {30}
            underlineColorAndroid="#3ABCA7"
            onChangeText = {(value)=>this.setState({text: value})}
            />
        </View>
          <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button}
            onPress={()=>this.addItem()}
            >
            <Text style={styles.plus}>+</Text>
          </TouchableOpacity>
          </View>
      </View>
    );
  }
}*/



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  todo: {
    width: Dimensions.get('window').width,
    height:55,
    backgroundColor:'#3ABCA7',
    marginTop: 10,
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
  },

  todoTitle:{
    fontSize:20,
    color:'#fff',
  },
  button: {
    height: 60,
    width: 60,
    backgroundColor: '#3ABCA7',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
  },
  plus: {
    color: '#fff',
    fontSize:20,
  },
  containerr: {
    display:'flex',
    flexDirection:'row',
    width: Dimensions.get('window').width,
    height:80,
    borderTopStyle:'solid',
    borderTopColor:'#3ABCA7',
    borderTopWidth:1,
  },
  inputContainer:{
    display:'flex',
    width: Dimensions.get('window').width-80,
    height:80,
    justifyContent:'flex-end',
    alignItems: 'center',
  },
  buttonContainer:{
    height: 80,
    width: 80,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    width: Dimensions.get('window').width-120,
    color: '#000',
    fontSize: 20,
    paddingBottom:10,
  }
});
