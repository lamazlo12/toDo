import React from 'react';
import { StyleSheet, View, StatusBar, Text } from 'react-native';
const Dimensions = require('Dimensions');
const window = Dimensions.get('window');

export default class Toolbar extends React.Component {
  render() {
    return (
      <View style={styles.toolbarContainer}>
        <StatusBar
          hidden={true}
          backgroundColor="blue"
          />
      <Text style={styles.appName}>ToDo</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
appName:{
  fontSize:30,
  color:'#3ABCA7',
},
toolbarContainer:{
  width:Dimensions.get('window').width,
  backgroundColor:'#fff',
  borderBottomStyle: 'solid',
  borderBottomWidth: 1,
  borderBottomColor: '#3ABCA7',
  display:'flex',
  alignItems:'center',
  justifyContent:'center',
  height:(Dimensions.get('window').height)/8,
}
});
