import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Toolbar from './components/toolbar';
import Todos from './components/todos';


export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Toolbar/>
        <Todos/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
